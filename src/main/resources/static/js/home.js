fetch('/posts')
    .then(response => response.json())
    .then(posts => {
        for (let post of posts) {
            document.getElementById('posts').innerHTML += `
                    <div>
                        <a href="/post.html?id=${post.id}" class="post">
                            <img src="${post.imageUrl}"  alt="image"/>
                            <div class="info">
                                <p>${post.title}</p>
                                <h3>${post.price}</h3>
                            </div>
                        </a>
                    </div>
                    `
        }
    });
