package shop.controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {

    private final SessionFactory sessionFactory;

    @Autowired
    public PostController(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @RequestMapping(produces = "application/json")
    public List getPosts() {
        Session session = sessionFactory.openSession();
        return session.createQuery("from Post").list();
    }

}
